package mocks

import "github.com/stretchr/testify/mock"

type MyInterface struct {
	mock.Mock
}

// DoSomething provides a mock function with given fields:
func (_m *MyInterface) DoSomething() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
