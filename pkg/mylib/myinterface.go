package mylib
  
// generate command for mockery
//go:generate mockery -name=MyInterface
// generate command for mockgen
//go:generate mockgen -destination=fake_myinterface.go -package=mylib bitbucket.org/robdaemon/gotools/pkg/mylib MyInterface

// MyInterface is a interface for example purposes only
type MyInterface interface {
  DoSomething() error
}
