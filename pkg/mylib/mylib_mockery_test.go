package mylib

import (
  "fmt"
  "testing"
  
  "github.com/stretchr/testify/assert"
  
  "bitbucket.org/robdaemon/gotools/pkg/mylib/mocks"
)

//START OMIT
func TestingMockery(t *testing.T) {
    iface := new(mocks.MyInterface)
    
    iface.On("DoSomething").Return(fmt.Errorf("Expected error"))
    
    err := iface.DoSomething()
    
    assert.NotNil(t, err)
    
    iface.AssertExpectations(t)
}
//END OMIT