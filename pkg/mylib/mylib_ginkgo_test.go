package mylib_test

import (
	. "bitbucket.org/robdaemon/gotools/pkg/mylib"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

// START OMIT
var _ = Describe("Mylib", func() {
  Describe("Adder should add properly", func() {
    Context("with five integers", func() {
      It("should equal 15", func() {
        Expect(AddNumbers(1, 2, 3, 4, 5)).To(Equal(15))
      })
    })
  })
})
// END OMIT