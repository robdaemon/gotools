package mylib

import "testing"

func TestAddNumbers(t *testing.T) {
    expected := 15
    result := AddNumbers(1, 2, 3, 4, 5)
    
    if result != expected {
        t.Errorf("Expected: %d, found %d", expected, result)
        t.Fail()
    }
}