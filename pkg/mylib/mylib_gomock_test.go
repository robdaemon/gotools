package mylib

import (
  "fmt"
  "testing"
  
  "github.com/golang/mock/gomock"
)

//START OMIT
func TestingGomock(t *testing.T) {
    ctrl := gomock.NewController(t)
    defer ctrl.Finish()
    iface := NewMockMyInterface(ctrl)
    
    iface.EXPECT().DoSomething().Return(fmt.Errorf("Expected error"))
    
    err := iface.DoSomething()
    if err == nil {
      t.Errorf("expected error")
      t.Fail()
    }
}
//END OMIT