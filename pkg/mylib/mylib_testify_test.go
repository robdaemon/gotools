package mylib

import (
    "testing"
    
    "github.com/stretchr/testify/assert"
)

func TestAddNumbers2(t *testing.T) {
    expected := 15
    result := AddNumbers(1, 2, 3, 4, 5)
    
    assert.Equal(t, expected, result, "Not what we wanted!")
}