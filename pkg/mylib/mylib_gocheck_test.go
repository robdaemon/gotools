package mylib

import (
    "testing"

    . "gopkg.in/check.v1"
)

// START OMIT
// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type MySuite struct{}

var _ = Suite(&MySuite{})

func (s *MySuite) TestAdder(c *C) {
    expected := 15
    result := AddNumbers(1, 2, 3, 4, 5)

    c.Assert(expected, Equals, result)
}
// END OMIT
