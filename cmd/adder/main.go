package main

import (
	"fmt"

	"bitbucket.org/robdaemon/gotools/pkg/mylib"
)

func main() {
	result := mylib.AddNumbers(1, 2, 3, 4, 5)
	fmt.Printf("result = %d\n", result)
}
