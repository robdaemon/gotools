# gotools

A short talk on various tooling within the Go ecosystem.

## License

This talk is licensed under Creative Commons CC0. Do what you wish.

See LICENSE or [Creative Commons](https://creativecommons.org/publicdomain/zero/1.0/)