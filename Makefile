ALLPKGS=$(shell go list ./... | grep -v /generated/)

.PHONY: clean all generate build test present tools

all: generate build test

clean:
		rm -rf build

generate:
		godep go generate $(ALLPKGS)
    
build:
		mkdir -p build/darwin-amd64 && cd build/darwin-amd64 && GOOS=darwin GOARCH=amd64 godep go build bitbucket.org/robdaemon/gotools/cmd/adder
		mkdir -p build/windows-amd64 && cd build/windows-amd64 && GOOS=windows GOARCH=amd64 godep go build bitbucket.org/robdaemon/gotools/cmd/adder
		mkdir -p build/linux-amd64 && cd build/linux-amd64 && GOOS=linux GOARCH=amd64 godep go build bitbucket.org/robdaemon/gotools/cmd/adder

test:
		godep go test ./...

present:
		cd slides && present

tools:
		go get -u golang.org/x/tools/cmd/present
		go get -u github.com/golang/mock/gomock
		go get -u github.com/golang/mock/mockgen
		go get -u github.com/vektra/mockery/cmd/mockery